<?php
    class Controller_admin_panel extends Controller
    {
        function __construct()
        {
            $this->model = new Model_admin_panel();
            $this->view = new View();
        }

        static function TestAuth()
        {
            session_start();

            if ($_SESSION['admin'] != "12345")
            {
		        session_destroy();
                header('Location:/admin_panel');
                exit();
            }
        }

        function action_index()
        {
            session_start();

		    if ($_SESSION['admin'] == "12345")
		    {
                if(isset($_GET['page'])) $page = $_GET['page'];
                if(isset($_GET['m'])) $module = $_GET['m'];
                if($page == null) $page = 1;
                
                switch($module)
                {
                    case 'gallery': 
                        $data = $this->model->get_data_from_table("photos", "*", $page);
                        $data['categ'] = $this->model->get_categories();
                        $this->view->generate('admin_gallery_view.php', 'admin_panel_view.php', $data);
                        break;
                    case "chat": 
                        $data = $this->model->get_data_from_table("messages", "*", $page);

                        $this->view->generate('admin_chat_view.php', 'admin_panel_view.php', $data);
                        break;
                    case "guest_book": 
                        $data = $this->model->get_data_from_table("guest_book", "*", $page);

                        $this->view->generate('admin_guest_book_view.php', 'admin_panel_view.php', $data);
                        break;
                    case "files": 
                        $data = $this->model->get_data_from_table("files", "*", $page);

                        $this->view->generate('admin_files_view.php', 'admin_panel_view.php', $data);
                        break;
                    default:
                        $data = $this->model->get_data_from_table("articles", "*", $page);
                        $data['categ'] = $this->model->get_categories();

			            $this->view->generate('admin_articles_view.php', 'admin_panel_view.php', $data);
                }

		    }
		    else
		    {
                $this->view->generate('', 'admin_panel_login_view.php', $data);
            }
        }

        public function action_login()
        {
            $data = $this->model->login();
            
            if($data['login_status'] == 'access_denied')
            $this->view->generate('', 'admin_panel_login_view.php', $data);
        }

        public function action_logout()
        {
            session_start();
		    session_destroy();
		    header('Location:/admin_panel');
        }

        public function action_delete_record()
        {
            Controller_admin_panel::TestAuth();

            $table = $_POST['table'];
            $id = $_POST['id'];

            $this->model->delete_data_from_table($table, $id);
        }

        public function action_change_record()
        {
            Controller_admin_panel::TestAuth();

            $table = $_POST['table_name'];
            $name = $_POST['val_names'];
            $value = $_POST['values'];
            $id = $_POST['id'];

            $image = null;
            $file = null;

            $vals = json_decode($value);
            $names = explode(",", $name);

            if(isset($_FILES['image']))
            {
                $uploaddir = 'images/';
                $uploadfile = "";
    
                $uploadfile = $uploaddir . basename($_FILES['image']['name']);
                if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile))
                array_push($vals, $uploadfile);
            } 
            else if(isset($_FILES['file']))
            {
                $uploaddir = 'files_store/';
                $uploadfile = "";
    
                $uploadfile = $uploaddir . basename($_FILES['file']['name']);
                if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile))
                array_push($vals, $uploadfile);
            } else 
            {
                array_pop($names);
            } 
            print_r($_POST);

            $this->model->update_data_in_table($table, $names, $id, $vals);
        }

        public function action_add_article()
        {
            Controller_admin_panel::TestAuth();

            $caption = $_POST['caption'];
            $content = $_POST['content'];
            $category = $_POST['category'];
            $image = null;

            if(isset($_FILES['img']))
            {
                $uploaddir = 'images/';
                $uploadfile = "";
    
                $uploadfile = $uploaddir . basename($_FILES['img']['name']);
                if(move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile))
                $image = $uploadfile;
            }

            $fields = ($image == null)? 'caption, content, category' : 'caption, content, category, img';
            $image = ($image != null)? ', "'.$image.'"' : '';

            $this->model->insert_data_to_table('articles', $fields, "'$caption', '$content', '$category'$image");
        }

        public function action_add_image()
        {
            Controller_admin_panel::TestAuth();

            $caption = $_POST['caption'];
            $category = $_POST['category'];
            $image = null;

            if(isset($_FILES['img']))
            {
                $uploaddir = 'images/';
                $uploadfile = "";
    
                $uploadfile = $uploaddir . basename($_FILES['img']['name']);
                if(move_uploaded_file($_FILES['img']['tmp_name'], $uploadfile))
                $image = $uploadfile;
            }

            $fields = 'name, category, path';

            $this->model->insert_data_to_table('photos', $fields, "'$caption', '$category', '$image'");
        }

        public function action_add_file()
        {
            Controller_admin_panel::TestAuth();

            $caption = $_POST['caption'];
            $description = $_POST['description'];
            $file = null;

            if(isset($_FILES['file']))
            {
                $uploaddir = 'files_store/';
                $uploadfile = "";
    
                $uploadfile = $uploaddir . basename($_FILES['file']['name']);
                if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile))
                $file = $uploadfile;
            }

            $fields = 'caption, description, path';

            $this->model->insert_data_to_table('files', $fields, "'$caption', '$description', '$file'");
        }
    }
?>