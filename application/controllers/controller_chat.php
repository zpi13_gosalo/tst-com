<?php
    class Controller_Chat extends Controller
    {
        function __construct()
        {
            $this->model = new Model_chat();
            $this->view = new View();
        }

        function action_index()
        {
            $this->view->generate('chat_view.php', 'template_view.php', $data);
        }

        public function action_load_message()
        {
            $this->model->Load();
        }

        public function action_send_message()
        {
            $this->model->Send();
        }
    }
?>