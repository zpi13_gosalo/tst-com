<?php
    class Controller_guest_book extends Controller
    {
        function __construct()
        {
            $this->model = new Model_guest_book();
            $this->view = new View();
        }

        function action_index()
        {
            $data = $this->model->get_data();
            $this->view->generate('guest_book_view.php', 'template_view.php', $data);
        }

        public function action_add_post()
        {
            $this->model->add_post();
            $data = $this->model->get_data();
            $this->view->generate('guest_book_view.php', 'template_view.php', $data);
        }
    }
?>