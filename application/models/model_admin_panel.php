<?php 

class Model_admin_panel extends Model
{
    private $conn;

    function __construct()
    {
        $this->conn = Model::db_connect();
    }

    public function login()
    {
        if(isset($_POST['login']) && isset($_POST['admin_pswd']))
		{
			$login = $_POST['login'];
			$password =$_POST['admin_pswd'];
			
			if($login=="admin" && $password=="12345")
			{
				$result["login_status"] = "access_granted";
				
				session_start(); echo $_SESSION['admin'];
				$_SESSION['admin'] = $password;
				header('Location:/admin_panel/');
			}
			else
			{
				$result["login_status"] = "access_denied";
			}
		}
		else
		{
			$result["login_status"] = null;
        }
        return $result;
    }

    public function get_data_from_table($table_name, $columns, $page)
    {
        $conn = $this->conn;

        $per_page = 10;
        $sql = "SELECT $columns from $table_name ";

        $start = abs(($page-1) * $per_page);
        $sql = $sql." ORDER BY id DESC LIMIT $start, $per_page ";
        $result['rows'] = $conn->query($sql);
        $sql_count = "SELECT count(*) from $table_name"; 

        $result['rows_count'] = $conn->query($sql_count)->fetch_array()[0];
        $result['per_page'] = $per_page;
        $result['current_page'] = $page;
        $result['table_name'] = $table_name;

        return $result;
    }

    public function get_categories()
    {
        $conn = $this->conn;

        $sql = "SELECT * FROM category";
        $result = $conn->query($sql);

        return $result;
    }

    public function insert_data_to_table($table_name, $columns, $values)
    {
        $conn = $this->conn;
        $sql = "INSERT INTO $table_name ($columns, date) VALUES ($values, curdate())";
        $conn->query($sql);
    }

    public function update_data_in_table($table_name, $names, $id, $values)
    {
        $conn = $this->conn;

        $sql = "UPDATE $table_name SET ";

        $count = count($names);

        for($i = 0; $i<$count; $i++)
        {
            $temp = (ctype_digit($values[$i]))? $values[$i] : '"'.$values[$i].'"';
            $sql = $sql.$names[$i].'='.$temp.', ';
        }

        $sql = $sql."date=curdate() WHERE id=$id";
        $conn->query($sql);

    }

    public function delete_data_from_table($table_name, $id)
    {
        $conn = $this->conn;

        $sql = "DELETE FROM $table_name WHERE id=$id";
        $conn->query($sql);
    }
}

?>