<?php 

class Model_files extends Model
{
    public function get_data()
    {
        $conn = Model::db_connect();
        $per_page = 10;

        $sql = 'SELECT caption, date, path, description FROM files ORDER BY id DESC ';
        $sql_count = 'SELECT COUNT(*) FROM files';

        $result['rows_count'] = $conn->query($sql_count)->fetch_array()[0];

        if(isset($_GET['page'])) $page = ($_GET['page']-1); 
        else $page = 0;

        $start = abs($page * $per_page);
        $sql = $sql."LIMIT $start, $per_page";

        $result['file_posts'] = $conn->query($sql);

        $result['per_page'] = $per_page;
        $result['current_page'] = $page+1;

        $conn->close();
        return $result;
    }

}

?>