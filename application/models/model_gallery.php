<?php 

class Model_gallery extends Model
{
    public function get_data()
    {
        $conn = Model::db_connect();

        $per_page = 27;

        $sql = 'SELECT name, description, path FROM photos ';

        if(isset($_GET['category']) && $_GET['category'] != 0) { 
            $cat = $_GET['category']; 
            $sql = $sql."WHERE category = $cat ";

            $sql_count = "SELECT COUNT(*) FROM photos WHERE category = $cat ";
            $result['rows_count'] = $conn->query($sql_count)->fetch_array()[0]; 
        }
        else
        {
            $sql_count = 'SELECT COUNT(*) FROM photos';
            $result['rows_count'] = $conn->query($sql_count)->fetch_array()[0];
        }

        if(isset($_GET['page'])) $page = ($_GET['page']-1); 
        else $page = 0;

        $start = abs($page * $per_page);
        $sql = $sql."LIMIT $start, $per_page";

        $result['photos'] = $conn->query($sql);

        $sql = "SELECT * FROM category";
        $result['categories'] = $conn->query($sql);
      
        $result['per_page'] = $per_page;
        $result['current_cat'] = $_GET['category'];
        $result['current_page'] = $page+1;

        $conn->close();
        return $result;
    }
}

?>