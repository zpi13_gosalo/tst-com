<?php 

class Model_guest_book extends Model
{
    public function get_data()
    {
        $conn = Model::db_connect();
        $per_page = 10;

        $sql = 'SELECT caption, date, poster_name, content, image FROM guest_book ORDER BY id DESC ';
        $sql_count = 'SELECT COUNT(*) FROM guest_book';

        $result['rows_count'] = $conn->query($sql_count)->fetch_array()[0];

        if(isset($_GET['page'])) $page = ($_GET['page']-1); 
        else $page = 0;

        $start = abs($page * $per_page);
        $sql = $sql."LIMIT $start, $per_page";

        $result['posts'] = $conn->query($sql);

        $result['per_page'] = $per_page;
        $result['current_page'] = $page+1;

        $conn->close();
        return $result;
    }

    public function add_post()
    {
        $conn = Model::db_connect();

        $caption = $_POST['caption'];
        $poster_name = $_POST['name'];
        $content = $_POST['content'];
    
        $sql = "INSERT INTO guest_book ";

        $image = null;

        if(isset($_FILES['image']))
        {
            $uploaddir = 'images/';
            $uploadfile = "";

            $uploadfile = $uploaddir . basename($_FILES['image']['name']);
            if(move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile))
            $image = $uploadfile;
        }

        $sql = $sql."(caption, date, poster_name, content, image) 
        VALUES ('$caption', curdate(), '$poster_name', '$content', '$image')";

        $conn->query($sql);
        $conn->close();
    }
}

?>