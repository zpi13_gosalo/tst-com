<div>
    <form action="/admin_panel/add_file" method="post" id="add_file" enctype="multipart/form-data">
        <input type='submit' action='submit' value='Добавить'>
        <input class ='caption' type='text' name='caption'>
        <textarea class ='content' name='description'></textarea>
        <span style='color: white; font-size: 0.7em;'> файл </span> <input type='file' name='file'>
    </form>
</div>
<div class = "pages">
    <div>
        <?php View::gen_pages_admin($data, "files"); ?>
    </div>
</div>
<?php
     $module = "files"; 
     while($news = $data['rows']->fetch_assoc())
          {
                $date = new DateTime($news['date']);
                $date_str = $date->format('d.m.y');
                $caption = $news['caption'];
                $path = $news['path'];
                $description = $news['description'];
                $id = $news['id'];

                echo "<div class='row' id='$id'>
                        <div class='redact' id='$id'>
                        <input type='button' id='$id' class='change_record' value='E' title='Редактировать'>
                        <input type='button' id='$id' class='delete_record' value='D' title='Удалить'>
                        <input type='button' id='$id' class='apply' value='А' title='Применить' disabled>
                        <input type='file' id='$id' class='change_image' name='file' title='Изменить файл' disabled>
                        </div>
                        <input class ='id' type='text' value='$id' readonly>
                        <input class ='caption' type='text' value='$caption' readonly>
                        <textarea class ='content' readonly>$description</textarea>
                        <input class ='path' type='text' value='$path' readonly>
                        <input class='date' type='text' value='$date_str' readonly>
                    </div>";
            }
?>
<div class = "pages">
    <div>
        <?php View::gen_pages_admin($data, "files"); ?>
    </div>
</div>