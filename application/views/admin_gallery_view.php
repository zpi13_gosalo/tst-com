<?php
    $categs = []; 
    while($row = $data['categ']->fetch_assoc()) array_push($categs, $row);

    $select = "<select name='category'>";
    foreach($categs as $cat)
    {
       $name = $cat['name'];
       $_id = $cat['id'];
       $select = $select."<option label='$name' value='$_id'></option>";
    }
    $select = $select."</select>";
?>
<div>
    <form action="/admin_panel/add_image" method="post" id="add_image" enctype="multipart/form-data">
        <input type='submit' action='submit' value='Добавить'>
        <input class ='caption' type='text' name='caption'>
        <?php echo $select?>
        <span style='color: white; font-size: 0.7em;'> изображение </span> <input type='file' name='img' id='add_img'>
    </form>
</div>
<div class = "pages">
    <div>
        <?php View::gen_pages_admin($data, "gallery"); ?>
    </div>
</div>
<?php
    $module = "gallery";   
    while($news = $data['rows']->fetch_assoc())
            { 
                $serv = $_SERVER['HTTP_HOST'];
                $img = $news['path'];

                $image = ($news['path'] == null)? $image = '' : 
                 "<div class='miniature'><img src= 'http://$serv/$img'></img></div>";
    
                 $date = new DateTime($news['date']);
                 $date_str = $date->format('d.m.y');
                 $caption = $news['name'];
                 $category = $news['category'];
                 $id = $news['id'];

                 $select = "<select disabled>";
                 foreach($categs as $cat)
                 {
                    $name = $cat['name'];
                    $_id = $cat['id'];
                    if($_id == $category) $select = $select."<option label='$name' value='$_id' selected></option>";
                    else $select = $select."<option label='$name' value='$_id'></option>";
                 }
                 $select = $select."</select>"; 

                 echo "<div class='row' id='$id'>
                        <div class='redact' id='$id'>
                        <input type='button' id='$id' class='change_record' value='E' title='Редактировать'>
                        <input type='button' id='$id' class='delete_record' value='D' title='Удалить'>
                        <input type='button' id='$id' class='apply' value='А' title='Применить' disabled>
                        <input type='file' id='$id' class='change_image' name='image' title='Сменить картинку' disabled>
                        </div>
                        <input class ='id' type='text' value='$id' readonly>
                        <input class ='caption' type='text' value='$caption' readonly>
                        $select
                        <input class='date' type='text' value='$date_str' readonly>
                        $image
                        </div>";
                    } 
?>
<div class = "pages">
    <div>
        <?php View::gen_pages_admin($data, "gallery"); ?>
    </div>
</div>