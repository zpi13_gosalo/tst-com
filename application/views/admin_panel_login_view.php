<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>TEST.com - вход в админ-панель</title>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <link href="http://test.com/styles/style_admin_panel.css" rel="stylesheet">
    </head>
    <body>
        
        <div class='login_form'>
            <form action='/admin_panel/login' method="post">
            <?php if($data['login_status'] == 'access_denied') 
            echo "<p>Не верные учетные данные!</p>" ?>
                <div><p>Вход в админ-панель</p></div>
                <p>Логин</p>
                <p><input id="inp" type="text" name="login"></p>
                <p>Пароль</p>
                <p><input id="inp" type="password" name="admin_pswd"></p>
                <p><input type="submit" value="Войти"></p>
            </form>
        </div>
    </body>
</html>