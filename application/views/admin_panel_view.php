<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>TEST.com - админ-панель</title>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <?php
            $server = 'http://'.$_SERVER['HTTP_HOST']; 
            echo "<link href='$server/styles/style_admin_panel.css' rel='stylesheet'>";
            echo "<script type='text/javascript' src = '$server/js/admin.js'></script>\n"; 
        ?>
        <link rel="icon" type="image/png" href="/fav_m.png" />
    </head>
    <body>
        <header>
            <h1>TEST.com админ-панель</h1>
        </header>
        <div class='r_menu'></div>
        <div class="container">
            <aside> 
                <div>
                    <span>Управление</span>
                </div>
                <a id="main" href="/admin_panel/">Главная/новости</a>
                <a id="gallery" href="/admin_panel?m=gallery">Галерея</a>
                <a id="chat" href="/admin_panel?m=chat">Чат</a>
                <a id="guest_book" href="/admin_panel?m=guest_book">Гостевая книга</a>
                <a id="files" href="/admin_panel?m=files">Каталог файлов</a>
                <a href="/admin_panel/logout">Выход</a>
            </aside>
            <div class="workfield">
                <div class = "pages">
                <div>
                    <?php include 'application/views/'.$content_view; ?>
                    <?php echo "<script language='javascript'>$('#$module').css('color', 'rgba(169, 169, 235, 0.918)');</script>" ?>
                    <?php echo "<script language='javascript'>var module_name = '$module';</script>" ?>
                </div>
            </div>
    </body>
</html>