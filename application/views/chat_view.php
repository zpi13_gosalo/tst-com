<?php View::gen_menu("chat"); ?>

<script type="text/javascript">

    $(() => {
        $("#pac_form").submit(Send); 
        $("#pac_text").focus();
        setInterval("Load();", 500);
    });

    function Load() 
    {
        if(!load_in_process)
        {
            load_in_process = true;
            $.post("chat/load_message",{ last: last_message_id, rand: (new Date()).getTime() },
                function (result) 
                {
                    var scroll = document.getElementById("chat_area").scrollHeight;
                    $(".chat").scrollTop(scroll);

                    load_in_process = false;
                });
        }
    }

    function Send() 
    {
        $.post("chat/send_message", { name: $("#pac_name").val(), text: $("#pac_text").val() }, Load);

        $("#pac_text").val("");
        $("#pac_text").focus();

        return false;
    }

    var last_message_id = 0; 
    var load_in_process = false; 
</script>

<div class = "container">
    <div style="padding: 100px;">
        <div class="chat">
            <div id="chat_area"></div>
        </div>
        <form id="pac_form" action="">
            <table style="width: 100%; margin-top: 10px;">
                <tr>
                    <td>Имя:</td>
                    <td>Сообщение:</td>
                    <td></td>
                </tr>
                <tr>
                    <td><input type="text" id="pac_name" class="r4" value="Гость"></td>
                    <td style="width: 80%;"><input type="text" id="pac_text" class="r4" value=""></td>
                    <td><input class="r4" type="submit" value="Отправить"></td>
                </tr>
            </table>
        </form>
    </div>
</div>


