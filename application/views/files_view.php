<?php View::gen_menu("files"); ?>
<div class = "container">
    <div class = "pages">
        <div>
            <?php View::gen_pages($data, "files"); ?>
        </div>
    </div>
    <section>
        <?php
            while($post_file = $data['file_posts']->fetch_assoc())
            {
                $date = new DateTime($post_file['date']);
                $date_str = $date->format('d.m.y');

                $file = $post_file['path'];
                $filename = explode('/', $file)[1];
                $host = 'http://'.$_SERVER['HTTP_HOST'];

                echo '<article><div class ="article_header"><h1>'.$post_file['caption'].
                     '</h1><span>Дата:  '.$date_str.'</span></div><p>'.$post_file['description'].
                     "</p><a href = '$host/$file' target='_blank' download='$filename'>Скачать с сервера</a></article>";
            }
        ?>
    </section>
    <div class = "pages">
        <div>
            <?php View::gen_pages($data, "files"); ?>
        </div>
    </div>
</div>


