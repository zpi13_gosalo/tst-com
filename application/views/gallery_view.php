<?php View::gen_menu("gallery"); ?>

<div class = "container">
    <aside>
            <header>Категории</header>
            <?php
                $id = 0;
                $name = "Все";
                $categ = ($data['current_cat'] !== null) ? $data['current_cat'] : 0;

                if($id == $categ) echo "<span><a id='focused' href='/gallery?category=$id'>$name</a></span>";
                else              echo "<span><a href='/gallery?category=$id'>$name</a></span>";

                while($cat = $data['categories']->fetch_assoc())
                {
                    $id = $cat['id'];
                    $name = $cat['name'];
                    if($id == $categ) echo "<span><a id='focused' href='/gallery?category=$id'>$name</a></span>";
                    else              echo "<span><a href='/gallery?category=$id'>$name</a></span>";
                } 
            ?>
    </aside>

    <div class = "pages">
        <div>
            <?php View::gen_pages($data, "gallery"); ?>
        </div>
    </div>

    <div class = "picture_grid">
        <?php
            while($pics = $data['photos']->fetch_assoc())
            { 
                $picture = $pics['path'];
                $pic_name = $pics['name'];
                $filename = explode('/', $picture)[1];
                $host = 'http://'.$_SERVER['HTTP_HOST'];
                
                echo "<div class ='picture_box'><h1>$pic_name</h1>".
                "<div><img src='$host/$picture'></img></div>".
                "<a href = '$host/$picture' target='_blank' download='$filename'>Сохранить</a>".
                "</div>";
            } 
        ?> 
    </div>

    <div class = "pages">
        <div>
            <?php View::gen_pages($data, "gallery"); ?>
        </div>
    </div>

</div>



