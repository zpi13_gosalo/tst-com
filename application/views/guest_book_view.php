<?php View::gen_menu("guest_book"); ?>
<div class = "container">
    <div class = "pages">
        <div>
            <?php View::gen_pages($data, "guest_book"); ?>
        </div>
    </div>
    <section>
        <?php
            while($posts = $data['posts']->fetch_assoc())
            { 
                $image = ($posts['image'] == null)? $image = '' : 
                '<div class="news_picture"><img src="'.
                'http://'.$_SERVER['HTTP_HOST'].'/'.$posts['image'].'"></img></div>';

                $date = new DateTime($posts['date']);
                $date_str = $date->format('d.m.y');

                echo '<article><div class ="article_header"><h1>'.$posts['caption'].'</h1><span>Добавил '.$posts['poster_name'].
                     '. Дата:  '.$date_str.'</span></div>'.$image.'<p>'.$posts['content'].'</p></article>';
            }
        ?>
    </section>
    <div class = "pages">
        <div>
            <?php View::gen_pages($data, "guest_book"); ?>
        </div>
    </div>
    <section>
        <div class='post_add'>
            <h1>Новый пост</h1>
            <form  id='post_form' enctype='multipart/form-data' action='/guest_book/add_post' method='post'>
                <p>Заголовок: </p><input id='inp' type='text' name='caption'>
                <p>Ваше имя: </p><input id='inp' type='text' name='name'>
                <p id='p'>Текст: </p><textarea id='mult' name='content'></textarea>
                <p>Изображение:</p> <input id='button_browse' name='image' type='file'>
                <input id='button_submit' type='submit' value='Отправить'>
            </form>
        </div>
    </section> 
</div>


