<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>TEST.com - тестовый сайт</title>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <?php
            $server = 'http://'.$_SERVER['HTTP_HOST']; 
            echo "<link href='$server/styles/style.css' rel='stylesheet'>" 
        ?>
        <link rel="icon" type="image/png" href="/fav_m.png" />
    </head>
    <body>
        <header><div class = "logo"><h1>TEST.com</h1></div></header>
        <?php include 'application/views/'.$content_view; ?>
        <footer>
            <span>Created by Anton Gosalo ZSTU 2017 (c)</span>
        </footer>     
    </body>
</html>