var table_list = 
{
    main: { name: 'articles', fields: 'caption,content,category,img' },
    gallery: { name: 'photos', fields: 'name,category,path' },
    chat: { name: 'messages' },
    guest_book: { name: 'guest_book', fields: 'caption,poster_name,content,image' },
    files: {name: 'files', fields: 'caption,description,path' }
};

$((function() {
    $('#add_article').on('submit', function(e){
        e.preventDefault();
        var $that = $(this),
        formData = new FormData($that.get(0));
        $.ajax({
          type: 'POST',
          contentType: false,
          processData: false,
          data: formData,
          url: '/admin_panel/add_article',
          success: function()
          {
            location.href = "/admin_panel?m="+module_name;
          }
        });
      });

      $('#add_image').on('submit', function(e){
        e.preventDefault();
        var $that = $(this),
        formData = new FormData($that.get(0));
        $.ajax({
          type: 'POST',
          contentType: false,
          processData: false,
          data: formData,
          url: '/admin_panel/add_image',
          success: function()
          {
            location.href = "/admin_panel?m="+module_name;
          }
        });
      });

      $('#add_file').on('submit', function(e){
        e.preventDefault();
        var $that = $(this),
        formData = new FormData($that.get(0));
        $.ajax({
          type: 'POST',
          contentType: false,
          processData: false,
          data: formData,
          url: '/admin_panel/add_file',
          success: function()
          {
            location.href = "/admin_panel?m="+module_name;
          }
        });
      });

    $('.change_record').on('click', function(e){
        panel = $('#' + e.target.id);

        caption = panel.find('.caption');
        content = panel.find('.content');
        poster_name = panel.find('.poster_name');
        select = panel.find('select');

        caption.removeAttr('readonly');
        content.removeAttr('readonly');
        poster_name.removeAttr('readonly');
        select.removeAttr('disabled');

        change_rec = $(this);
        delete_rec = panel.find('.delete_record');
        change_image = panel.find('.change_image');
        apply = panel.find('.apply');

        change_rec.attr('disabled', 'disabled');
        apply.removeAttr('disabled');
        change_image.removeAttr('disabled');

        change_image.on('click', function(e){
            
        });
    
        apply.on('click', function(){
            t_name = table_list[module_name].name;
            fields = table_list[module_name].fields;

            _values = [];

            switch(module_name)
            {
              case 'main':
                _values.push(caption.val());
                _values.push(content.val());
                _values.push(select.val());
              break;
              case 'gallery': 
                _values.push(caption.val());
                _values.push(select.val());
              break;
              case 'guest_book': 
                _values.push(caption.val());
                _values.push(poster_name.val());
                _values.push(content.val());
              break;
              case 'files':
                _values.push(caption.val());
                _values.push(content.val()); 
              break;
            }

            formData = new FormData();

            formData.append(change_image.attr('name'),  change_image[0].files[0]);
            formData.append('table_name', t_name);
            formData.append('val_names', fields);
            formData.append('values', JSON.stringify(_values));
            formData.append('id', $(this).attr("id"));

            $.ajax({
              type: "POST",
              url: "/admin_panel/change_record",
              async: true,
              data: formData,
              cache: false,
              contentType: false,
              processData: false
            });

            change_rec.removeAttr('disabled');
            apply.attr('disabled', 'disabled');
            change_image.attr('disabled', 'disabled');
            caption.attr('readonly', 'readonly');
            content.attr('readonly', 'readonly');
            poster_name.attr('readonly', 'readonly');
            select.attr('disabled', 'disabled');
            if(selected != undefined) selected.css('opacity', '0');
        });
    
    });
    var selected = undefined;

    $('.id').on('click', function(e){

        if(selected != undefined) selected.css('opacity', '0');
        id = $(this).val();
        selected = $('#'+id+'.redact');
        selected.css('opacity', '1');
    });


    $('.delete_record').on('click', function(e){
        t_name = table_list[module_name].name;
        panel = $('#' + e.target.id);
        $.post( "/admin_panel/delete_record", { table: t_name, id: $(this).attr("id") } );
        panel.css('display', 'none');
    });
    
}));